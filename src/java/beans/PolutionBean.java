/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import enitities.Polution;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import model.OracleGetPolutions;

@ManagedBean(name = "polutionBean")
@RequestScoped
public class PolutionBean implements Serializable {

    private String station;
    private String date;
    private String value;
    private String pollutant;

    private List<Polution> list;
    private List<Polution> filteredList;
    private List<String> stations;
    private List<String> years;

    private String option;

    /**
     * Creates a new instance of PolutionBean
     */
    public PolutionBean() {
        list = new ArrayList<>();
        filteredList = new ArrayList<>();
        stations = new ArrayList<>();
        stations.add("Nice Arson");
        stations.add("A?roport de Nice");
        stations.add("Peillon");
        stations.add("Nice Ouest Botanique");
        stations.add("Nice Promenade des Anglais");
        stations.add("Contes 2");

        years = new ArrayList<>();
        years.add("2014");
        years.add("2015");
        years.add("2016");

    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

/*    @PostConstruct
    public void loadAll() {
        list = OracleGetPolutions.getAll("2", "2014", "Contes 2");
    }*/

    public List<Polution> getList() {
       if (list.isEmpty()) {
            refreshCache();
    }
       return list;
    }
       
         public void refreshCache() {
        list = OracleGetPolutions.getAll("2", "2014", "Contes 2");
    }


    public void setList(List<Polution> list) {
        this.list = list;
    }

    public List<Polution> getFilteredList() {
        return filteredList;
    }

    public void setFilteredList(List<Polution> filteredList) {
        this.filteredList = filteredList;
    }

    public List<String> getStations() {
        return stations;
    }

    public void setStations(List<String> stations) {
        this.stations = stations;
    }

    public String getPollutant() {
        return pollutant;
    }

    public void setPollutant(String pollutant) {
        this.pollutant = pollutant;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public void update(String value, String month, String year) {

        System.out.println("HEEEEEY UPDATE EEEEE " + value);
        list = OracleGetPolutions.getAll(month, years.get(Integer.valueOf(year)-1), stations.get(Integer.valueOf(value) - 1));
    }

}
