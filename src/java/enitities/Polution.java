/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enitities;

/**
 *
 * @author user
 */
public class Polution {
    private String station;
    private String date;
    private String value;
    private String pollutant;

    public Polution(String station, String date, String value, String pollutant) {
        this.station = station;
        this.date = date;
        this.value = value;
        this.pollutant = pollutant;
    }
    
     public Polution(String station, String date) {
        this.station = station;
        this.date=date;
        this.value="";
    }


    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPollutant() {
        return pollutant;
    }

    public void setPollutant(String pollutant) {
        this.pollutant = pollutant;
    }
    
}
